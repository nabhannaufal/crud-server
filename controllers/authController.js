const { users } = require("../models");
function format(user) {
  const { id, username, role } = user;
  return {
    id,
    username,
    role,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  register: (req, res, next) => {
    if (!req.body.username || !req.body.email || !req.body.password) {
      res.status(400).json({
        result: "FAILED",
        message: "username or email or password field cannot be empty.",
      });
    } else {
      users
        .register(req.body)
        .then(() => {
          res.status(200).json({ message: "Register success" });
        })
        .catch((err) => {
          res.status(500).json({
            result: "FAILED",
            message: err.message || "Error while create users",
          });
        });
    }
  },
  login: (req, res) => {
    if (!req.body.username || !req.body.password) {
      res.status(400).json({
        result: "FAILED",
        message: "username or password field cannot be empty.",
      });
    } else {
      users
        .authenticate(req.body)
        .then((user) => {
          res.status(200).json(format(user));
        })
        .catch((err) => {
          res.status(500).json({
            result: "FAILED",
            message: err.message || "invalid username or password",
          });
        });
    }
  },
  profil: (req, res) => {
    const currentUser = req.user;
    res
      .status(200)
      .json(currentUser)
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err.message || "error while get data",
        });
      });
  },
};
