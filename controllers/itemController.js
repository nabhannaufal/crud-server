const { items } = require("../models");

module.exports = {
  getItem: (req, res) => {
    items
      .findAll()
      .then((data) => {
        res.json(data);
      })
      .catch((err) => {
        res.json(err);
      });
  },

  addItem: (req, res) => {
    items
      .create(req.body)
      .then((data) => {
        res.status(201).json({
          result: "Success add item",
          message: data,
        });
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err.message || "Some error occurred while creating item",
        });
      });
  },

  updateItem: (req, res) => {
    const id = req.params.id;
    items
      .update(req.body, {
        where: { id: id },
      })
      .then((num) => {
        if (num == 1) {
          res.status(200).json({
            result: "SUCCESS",
            message: "Item was updated successfully.",
          });
        } else {
          res.status(400).json({
            result: "FAILED",
            message: `Cannot update item with id=${id}. Maybe Item was not found or req.body is empty!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: "Error updating item with id=" + id,
        });
      });
  },

  getItemById: (req, res) => {
    const id = req.params.id;
    items
      .findByPk(id, { include: ["purchase_item"] })
      .then((data) => {
        if (data !== null && data !== undefined) {
          res.status(200).json({
            result: "SUCCESS",
            message: data,
          });
        } else {
          res.status(400).json({
            result: "FAILED",
            message: `Cannot find item with id=${id}`,
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: "Error retrieving item with id=" + id,
        });
      });
  },

  deleteItem: (req, res) => {
    const id = req.params.id;
    items
      .destroy({
        where: { id: id },
      })
      .then((num) => {
        if (num == 1) {
          res.status(200).json({
            result: "SUCCESS",
            message: "Item was deleted successfully!",
          });
        } else {
          res.status(400).json({
            result: "FAILED",
            message: `Cannot delete item with id=${id}. Maybe Player was not found!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: "Could not delete item with id=" + id,
        });
      });
  },
};
