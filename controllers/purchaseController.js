const { purchase_items, items } = require("../models");
module.exports = {
  getPurchaseItem: (req, res) => {
    purchase_items
      .findAll({ include: ["item"] })
      .then((data) => {
        res.json(data);
      })
      .catch((err) => {
        res.json(err);
      });
  },

  addPurchaseItem: (req, res) => {
    purchase_items
      .create(req.body)
      .then((data) => {
        res.status(201).json({
          result: "Success add purchase item",
          message: data,
        });
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message:
            err.message || "Some error occurred while creating purchase item",
        });
      });
  },

  checkoutPurchaseItem: (req, res) => {
    const item_id = req.body.item_id;
    const quantity = parseInt(req.body.quantity);
    items
      .findByPk(item_id)
      .then((item) => {
        const getStock = parseInt(item.stock);
        if (quantity < getStock) {
          const newStock = getStock - quantity;
          console.log("a", newStock);
          items
            .update(
              { stock: newStock },
              {
                where: { id: item_id },
              }
            )
            .then(
              purchase_items.create(req.body).then((data) => {
                res.status(201).json({
                  result: "Success add purchase item",
                  message: data,
                });
              })
            );
        } else {
          res.status(400).json({
            result: "FAILED",
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err,
        });
      });
  },

  updatePurchaseItem: (req, res) => {
    const id = req.params.id;
    purchase_items
      .update(req.body, {
        where: { id: id },
      })
      .then((num) => {
        if (num == 1) {
          res.status(200).json({
            result: "SUCCESS",
            message: "Item was updated successfully.",
          });
        } else {
          res.status(400).json({
            result: "FAILED",
            message: `Cannot update purchase item with id=${id}. Maybe it was not found or req.body is empty!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: "Error updating purchase item with id=" + id,
        });
      });
  },

  getPurchaseItemById: (req, res) => {
    const id = req.params.id;
    purchase_items
      .findByPk(id, { include: ["item"] })
      .then((data) => {
        if (data !== null && data !== undefined) {
          res.status(200).json({
            result: "SUCCESS",
            message: data,
          });
        } else {
          res.status(400).json({
            result: "FAILED",
            message: `Cannot find purchase item with id=${id}`,
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: "Error retrieving purchase item with id=" + id,
        });
      });
  },

  deletePurchaseItem: (req, res) => {
    const id = req.params.id;
    purchase_items
      .destroy({
        where: { id: id },
      })
      .then((num) => {
        if (num == 1) {
          res.status(200).json({
            result: "SUCCESS",
            message: "Purchase item was deleted successfully!",
          });
        } else {
          res.status(400).json({
            result: "FAILED",
            message: `Cannot delete purchase item with id=${id}. Maybe Player was not found!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: "Could not delete purchase item with id=" + id,
        });
      });
  },
};
