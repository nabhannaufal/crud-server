const router = require("express").Router();
const auth = require("./controllers/authController");
const item = require("./controllers/itemController");
const purchase = require("./controllers/purchaseController");
const restrict = require("./middlewares/restrict");

//auth api
router.post("/api/auth/register", auth.register);
router.post("/api/auth/login", auth.login);
router.get("/api/auth/profil", restrict, auth.profil);

//item api
router.get("/api/item", restrict, item.getItem);
router.post("/api/item/add", restrict, item.addItem);
router.put("/api/item/:id", restrict, item.updateItem);
router.get("/api/item/:id", restrict, item.getItemById);
router.delete("/api/item/:id", restrict, item.deleteItem);

//purchase_item api
router.get("/api/purchase", restrict, purchase.getPurchaseItem);
router.post("/api/purchase/add", restrict, purchase.addPurchaseItem);
router.post("/api/purchase/checkout", restrict, purchase.checkoutPurchaseItem);
router.put("/api/purchase/:id", restrict, purchase.updatePurchaseItem);
router.get("/api/purchase/:id", restrict, purchase.getPurchaseItemById);
router.delete("/api/purchase/:id", restrict, purchase.deletePurchaseItem);

module.exports = router;
