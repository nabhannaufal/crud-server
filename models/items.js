"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class items extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      items.hasMany(models.purchase_items, {
        foreignKey: "item_id",
        as: "purchase_item",
      });
    }
  }
  items.init(
    {
      name: DataTypes.STRING,
      stock: DataTypes.BIGINT,
      price: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "items",
    }
  );
  return items;
};
