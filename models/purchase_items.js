"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class purchase_items extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      purchase_items.belongsTo(models.items, {
        foreignKey: "item_id",
        as: "item",
      });
    }
  }
  purchase_items.init(
    {
      item_id: DataTypes.BIGINT,
      quantity: DataTypes.BIGINT,
      date: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "purchase_items",
    }
  );
  return purchase_items;
};
