"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "purchase_items",
      [
        {
          item_id: 5,
          quantity: 1,
          date: "2021-05-01",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          item_id: 3,
          quantity: 3,
          date: "2021-05-01",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          item_id: 2,
          quantity: 7,
          date: "2021-05-01",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("purchase_items", null, {});
  },
};
