"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "items",
      [
        {
          name: "Samsung",
          stock: 11,
          price: 1000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Huawei",
          stock: 12,
          price: 2000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Iphone",
          stock: 13,
          price: 3000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Sony",
          stock: 14,
          price: 4000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "LG",
          stock: 15,
          price: 5000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("items", null, {});
  },
};
