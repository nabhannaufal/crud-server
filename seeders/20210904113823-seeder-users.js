"use strict";
const bcrypt = require("bcrypt");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "users",
      [
        {
          username: "admin",
          password: bcrypt.hashSync("admin", 10),
          email: "admin@mail.com",
          role: "admin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "user",
          password: bcrypt.hashSync("user", 10),
          email: "user1@mail.com",
          role: "regular",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "nabhan",
          password: bcrypt.hashSync("nabhan", 10),
          email: "nabhan@mail.com",
          role: "regular",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
