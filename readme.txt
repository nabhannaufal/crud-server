Instalation
A) Server (https://gitlab.com/nabhannaufal/crud-server)
    1) go to directory
    2) install module (npm install)
    3) Set config.json in folder config, (default i'm using postgre)
    4) run sequelize:
        sequelize db:create
        sequelize db:migrate
        sequelize db:seed:all
    5) run server with command:
        npm run dev || node server.json

B) Client (https://gitlab.com/nabhannaufal/crud-client)
    1) go to directory
    2) install module (npm install)
    3) run client app: 
        npm start