const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../server");
const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywidXNlcm5hbWUiOiJuYWJoYW4iLCJpYXQiOjE2MzA5OTY4OTd9.4IBEkFZb2aRcRuqjxNcigLdx6ok0rGFHufvrU8drijY";

chai.use(chaiHttp);
chai.should();

describe("Unit Testing", () => {
  describe("GET /api/auth/profil", () => {
    it("should get data profil", (done) => {
      chai
        .request(app)
        .get("/api/auth/profil")
        .set({ authorization: token })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
    it("should get all purchase list", (done) => {
      chai
        .request(app)
        .get("/api/purchase")
        .set({ authorization: token })
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
    it("should get all item list", (done) => {
      chai
        .request(app)
        .get("/api/item")
        .set({ authorization: token })
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
    it("should get all purchase list by id", (done) => {
      const id = 1;
      chai
        .request(app)
        .get(`/api/purchase/${id}`)
        .set({ authorization: token })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
    it("should get all item list by id", (done) => {
      const id = 1;
      chai
        .request(app)
        .get(`/api/item/${id}`)
        .set({ authorization: token })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
    it("should add new user", (done) => {
      chai
        .request(app)
        .post("/api/auth/register")
        .send({ username: "user3", email: "user3@mail.com", password: "user" })
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
    it("should get token after user login", (done) => {
      chai
        .request(app)
        .post("/api/auth/login")
        .send({ username: "admin", password: "admin" })
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});
